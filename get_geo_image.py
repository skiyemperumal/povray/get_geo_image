#!/usr/bin/python
import numpy as np
from ase import Atoms
from ase.io import write, read
from ase.io.pov import get_bondpairs
import commands, os, re
from ase.data import covalent_radii
from ase.data.colors import jmol_colors

def settings():
  global kwargs
  kwargs = {
      'rotation'      : rot, # text string with rotation (default='' )
      'radii'         : radii, # float, or a list with one float per atom
      'colors'        : colors, #[(192,192,192),(96,96,96)],# List: one (r, g, b) tuple per atom
      'show_unit_cell': 0,   # 0, 1, or 2 to not show, show, and show all of cell
      }
  
  # Extra kwargs only available for povray (All units in angstrom)
  kwargs.update({
      'run_povray'   : False, # Run povray or just write .pov + .ini files
      'display'      : False,# Display while rendering
      'pause'        : True, # Pause when done rendering (only if display)
      'transparent'  : False,# Transparent background
      'canvas_width' : None, # Width of canvas in pixels
      'canvas_height': 1000, # Height of canvas in pixels 
      'camera_dist'  : 10.,  # Distance from camera to front atom
      'image_plane'  : None, # Distance from front atom to image plane
      'camera_type'  : 'orthographic', # perspective, ultra_wide_angle
      'point_lights' : [],             # [[loc1, color1], [loc2, color2],...]
      'area_light'   : [(2.0, 3.0, 40.0), # location
                        'White',       # color
                        .7, .7, 3, 3], # width, height, Nlamps_x, Nlamps_y
      'background'   : 'White',        # color
      'textures'     : None, # Length of atoms list of texture names
      'celllinewidth': 0.04,  # Radius of the cylinders representing the cell
      'bondlinewidth'  : 0.07, # Radius of the cylinders representing the bonds 
      'bondatoms'      : bonds,   # [[atom1, atom2], ... ] pairs of bonding atoms 
      'exportconstraints' : False
      })
     
if os.getcwd()[28] == 'v':
  with open("OUTCAR","r") as f:
     for n, line in enumerate(f):
           if "reached r" in line:
              sf = read("CONTCAR")
  
  #filename setting
  bonds = get_bondpairs(sf,radius=0.7)
  
  #Manually finding the bonds that are missed by the radius=0.7 threshold
  for n, i in enumerate(sf):
    if i.number == 8:
      for m, j in enumerate(sf):
        if j.number == 22:
  	  bd = sf.get_distance(n,m)
          if 2.2 < bd < 2.6:
  	    bonds.append([n,m])
        if j.number == 29:
  	  bd = sf.get_distance(n,m)
          if 1.8 < bd < 2.4:
  	    bonds.append([n,m])
  
  rot = '-87x,-6y,0z'  # found using ag: 'view -> rotate'
  radii = []; colors = []
  for i in sf:
    if i.number == 29 or i.number ==78:
      radii.append(covalent_radii[i.number]-0.77)
      colors.append(jmol_colors[i.number])
    if i.number == 22:
      radii.append(covalent_radii[i.number]-1.10)
      colors.append(jmol_colors[i.number])
    if i.number == 8:
      radii.append(covalent_radii[i.number]-0.30)
      if i.z > 17.9:
        colors.append([0.02,0.02,0.98])
      else:
        colors.append(jmol_colors[i.number])
    if i.number == 6:
      radii.append(covalent_radii[i.number]-0.35)
      colors.append([0.02,0.98,0.02])
  settings() 

if os.getcwd()[28] == 'c':
  #Read optimized xyz file
  file = commands.getstatusoutput('ls -t cp2k-*.xyz |head -1')[1]
  sf = read("%s" %file)
  rot = '-86x,-7y,0z'
  radii = []; colors = []
  for i in sf:
    if i.number == 29:
      radii.append(covalent_radii[i.number]-0.77)
      colors.append([.87,.46,0])
    if i.number == 22:
      radii.append(covalent_radii[i.number]-1.10)
      colors.append(jmol_colors[i.number])
    if i.number == 8:
      radii.append(covalent_radii[i.number]-0.30)
      if i.z > 20.5 or i.index == 287:
        colors.append([1.0,0,1.0])
      else:
        colors.append(jmol_colors[i.number])
    if i.number == 6:
      radii.append(covalent_radii[i.number]-0.35)
      colors.append([0,.85,1.0])
  bonds = get_bondpairs(sf,radius=0.7)
  for n, i in enumerate(sf):
    if i.number == 29 or i.number == 22:
      for m, j in enumerate(sf):
        if j.number == 8:
          bd = sf.get_distance(n,m)
          if 2.0 < bd < 2.4:
            bonds.append([n,m])
  bonds.append([289,44])
  settings()
